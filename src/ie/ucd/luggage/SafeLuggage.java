package ie.ucd.luggage;

import java.util.ArrayList;
import java.util.List;

public class SafeLuggage extends Luggage {
	
	private String password = "admin";
	private double bagWeight = 5;
	
	private List<Item> items;
	
	public SafeLuggage(){
		items = new ArrayList<Item>();
	}
	
	public void add(Item item, String password){
		if(this.password == password) {
			items.add(item);
		}
		else {
			System.out.println("Invalid Password!");
		}
	}
	
	public void removeItem(int index, String password){
		if(!items.isEmpty()) {
			if(this.password == password) {
				items.remove(index);
			}
			else {
				System.out.println("Invalid Password!");
			}
		}
		else {
			System.out.println("Empty Luggage!");
		}
	}

	@Override
	public double getBagWeight() {
		// TODO Auto-generated method stub
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		// TODO Auto-generated method stub
		return 25;
	}
	
	

}

package ie.ucd.luggage;

public class Suitcase extends Luggage {
	
	private double bagWeight = 5;

	@Override
	public double getBagWeight() {
		// TODO Auto-generated method stub
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		// TODO Auto-generated method stub
		return 25;
	}
	
	public Suitcase(double bagWeight) {
		this.bagWeight = bagWeight;
	}

}
